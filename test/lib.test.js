import test from 'ava'
import {dice, launch, sameFace, isolatedFace, pair, brelan, square, quinte, sixte, suite, pickRandom, launchRandom} from '../src/lib.js'

test("dice", t => t.deepEqual([1, 2, 3, 4, 5, 6], dice))
test("launch 2 dices", t => t.deepEqual([[1], [1]], launch(2)([1])))
test("pick a random face", t => t.true([1, 2, 3, 4, 5, 6].includes(pickRandom([1, 2, 3, 4, 5, 6]))))
test("shuffle a dice", t => t.notDeepEqual([1, 2, 3, 4, 5, 6], launchRandom([1, 2, 3, 4, 5, 6])))
test("same face of a dice", t => t.is(2, sameFace(1)([1, 1])))
test("one and only one face of a dice", t => t.true(isolatedFace(1)([1, 2, 2])))
test("has pair of 2", t => t.true(pair(2)([1, 2, 2])))
test("has brelan of 6", t => t.true(brelan(6)([1, 2, 5, 6, 6, 6])))
test("has square of 1", t => t.true(square(1)([1, 1,1,1,2,4])))
test("has quinte of 3", t => t.true(quinte(3)([3,3,3,3, 3, 1])))
test("has sixte of 3", t => t.true(sixte(3)([3, 3, 3, 3, 3, 3])))
test("has suite of at least 3 dices", t => t.true(suite(3)([1, 2, 3, 3, 3, 3])))
