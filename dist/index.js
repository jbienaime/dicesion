(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('ramda'), require('lodash.shuffle')) :
	typeof define === 'function' && define.amd ? define(['exports', 'ramda', 'lodash.shuffle'], factory) :
	(factory((global.dicesion = {}),global.R,global.shuffle));
}(this, (function (exports,R,shuffle) { 'use strict';

	shuffle = shuffle && shuffle.hasOwnProperty('default') ? shuffle['default'] : shuffle;

	/**
	 * @author jbienaime<jerome.bienaime@gmail.com>
	 */

	/** An array that goes from 1 to 6.
	 * 
	 * @const dice
	 * @type number[]
	 * @example ```
	 * > dice
	 * [1, 2, 3, 4, 5, 6]
	 * ```
	 */
	const dice = R.range(1, 7);

	/** Launch x times a same dice.
	 * 
	 * @param {number} diceNumber number of dice launches 
	 * @param {number[]} dice array that represents a dice
	 * @return {number[][]} array of dice, each index representing a launch
	 * @example ```
	 * > launch(2)(dice)
	 * [[1, 2, 3, 4, 5 ,6], [1, 2, 3, 4, 5, 6]]
	 * > launch(4)([1, 2])
	 * [[1, 2], [1, 2], [1, 2], [1, 2]]
	 * ```
	 */
	const launch        = diceNumber => dice => Array(diceNumber).fill(dice);

	/** Pick a random face in a dice
	 * 
	 * @param {number[]} list array that represents a dice
	 * @return {number} random face of the dice
	 * @example ```
	 * > pickRandom([1, 3, 5])
	 * 3
	 * > pickRandom([1, 4, 6, 3, 1])
	 * 6
	 * ```
	 */
	const pickRandom    = list => R.compose(R.head, shuffle)(list);

	/** Set a dice given an range. Will shuffle the dice each time.
	 * 
	 * @param {number} diceNumber
	 * @param {number[]} range array that represent a dice
	 * @return {number[][]} array of dice, each index representing a launch
	 * @example ```
	 * > launchRandom(4, dice)
	 * [1, 1, 4, 6]
	 * > launchRandom(2, [1, 3, 5])
	 * [1, 3]
	 * > launchRandom(6, dice)
	 * [1, 2, 1, 5, 1]
	 * ``` 
	 */
	const launchRandom  = diceNumber => list => Array(diceNumber).fill(0).map(() => pickRandom(list));

	/** Number of times given face in the dice
	 * 
	 * @param {number} face 
	 * @param {number[]} dice
	 * @return {number} - number of times face is appearing
	 * @example ```
	 * > sameFace(1)([3, 4, 5])
	 * 0
	 * > sameFace(3)([2, 1, 3, 3])
	 * 2
	 * > sameFace(2)(dice)
	 * 1
	 * ```
	 */
	const sameFace     = face => dice => R.length(R.filter(R.equals(face))(dice));

	/** Tells if a face is isolated 
	 * 
	 * @param {number} face
	 * @param {number[]} dice
	 * @return {boolean} true if isolated, false otherwise 
	 * @example ```
	 * > isolatedFace(1)([1, 2, 3, 4])
	 * true
	 * > isolatedFace(1)([3, 4])
	 * false
	 * ```
	 */
	const isolatedFace  = face => dice => R.equals(1)(sameFace(face)(dice));

	/** Tells if dice have pair of the given face
	 * 
	 * @param {number} face
	 * @param {number[]} dice
	 * @return {boolean} - true if pair, false otherwise
	 * @example ```
	 * > pair(1)([1, 1, 2])
	 * true
	 * > pair(5)([6, 6, 2, 3])
	 * false
	 * ```
	 */
	const pair          = face => dice => R.equals(2)(sameFace(face)(dice));

	/** Tells if dice have brelan of the given face
	 * 
	 * @param {number} face
	 * @param {number[]} dice
	 * @return {boolean} - true if brelan, false otherwiseé
	 */
	const brelan        = face => dice => R.equals(3)(sameFace(face)(dice));

	/** Tells if dice have square of the given face
	 * 
	 * @param {number} face
	 * @param {number[]} dice
	 * @return {boolean} true if square, false otherwise
	 * @example ```
	 * > square(5)([5, 5, 5, 5, 1])
	 * true
	 * > square(5)(dice)
	 * false
	 * ```
	 */
	const square        = face => dice => R.equals(4)(sameFace(face)(dice));

	/** Tells if dice have quinte of the given face
	 * 
	 * @param {number} face
	 * @param {number[]} dice
	 * @return {boolean} true if quinte, false otherwise
	 * @example ```
	 * > quinte(2)([2, 2, 2, 2, 2, 1])
	 * true
	 * > quinte(3)(dice)
	 * false
	 * ```
	 */
	const quinte        = face => dice => R.equals(5)(sameFace(face)(dice));

	/** Tells if dice have sixte of the given face
	 * 
	 * @param {number} face
	 * @param {number[]} dice
	 * @return {boolean} true if sixte, false otherwise
	 * @example ```
	 * > sixte(3)([3, 3, 3, 3, 3, 3])
	 * true
	 * > sixte(5)(dice)
	 * false
	 * ```
	 */
	const sixte         = face => dice => R.equals(6)(sameFace(face)(dice));

	/** Tells if dice have suite of count following faces
	 * 
	 * @param {number} count
	 * @param {number[]} dice
	 * @return {boolean} true if suite of counted dice, false otherwise
	 * @example ```
	 * > suite(3)([1, 2, 3])
	 * true
	 * > suite(3)([1, 2])
	 * false
	 * > suite(5)([1, 2, 3, 4, 5, 6])
	 * true
	 * ```
	 */
	const suite    = count => dice => R.gte(count)(R.length(R.uniq(dice)));

	exports.dice = dice;
	exports.launch = launch;
	exports.pickRandom = pickRandom;
	exports.launchRandom = launchRandom;
	exports.sameFace = sameFace;
	exports.isolatedFace = isolatedFace;
	exports.pair = pair;
	exports.brelan = brelan;
	exports.square = square;
	exports.quinte = quinte;
	exports.sixte = sixte;
	exports.suite = suite;

	Object.defineProperty(exports, '__esModule', { value: true });

})));
