dicesion
---

Common results on a dice.

Given a dice composed of numbers, return combinations.



## Quick usage
```js
const C = require('dicesion')

// Does set contains a pair of 1?
console.log(C.pair(1)([1, 2, 1])) // true

// Does set contains a brelan of 3?
console.log(C.brelan(3)([1, 2, 3, 4])) // false
```

## API

[TODO]

Meanwhile, the sources are documented

## Build sources

```bash
> yarn install
> yarn build
```

## Test

```bash
> yarn install
> yarn test
# or with coverage
> yarn test:cover
```

## LICENSE

GPL-2.0 see LICENSE File
